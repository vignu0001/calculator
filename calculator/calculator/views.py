from django.template import Context,loader,RequestContext
from django.shortcuts import get_object_or_404,render
from django.forms import ModelForm
from django.http import HttpResponse, Http404,HttpResponseRedirect,HttpResponseNotFound
from news.views import calculat

def calculator(request):
	if request.POST:
		if '0' in request.POST.keys():
			b = request.POST['valueof']
			a = 0
			return render(request,'news/calculator.html',locals())
			
		elif '1' in request.POST.keys():
			b = request.POST['valueof']
			a =1
			return render(request,'news/calculator.html',locals())
			
		elif '2' in request.POST.keys():
			b = request.POST['valueof']
			a=2
			return render(request,'news/calculator.html',locals())
			
		elif '3' in request.POST.keys():
			b = request.POST['valueof']
			a=3
			return render(request,'news/calculator.html',locals())
			
		elif '4' in request.POST.keys():
			b = request.POST['valueof']
			a=4
			return render(request,'news/calculator.html',locals())
			
		elif '5' in request.POST.keys():
			b = request.POST['valueof']
			a=5
			return render(request,'news/calculator.html',locals())
			
		elif '6' in request.POST.keys():
			b = request.POST['valueof']
			a=6
			return render(request,'news/calculator.html',locals())
			
		elif '7' in request.POST.keys():
			b = request.POST['valueof']
			a=7
			return render(request,'news/calculator.html',locals())
			
		elif '8' in request.POST.keys():
			b = request.POST['valueof']
			a=8
			return render(request,'news/calculator.html',locals())
			
		elif '9' in request.POST.keys():
			b = request.POST['valueof']
			a=9
			return render(request,'news/calculator.html',locals())
			
		elif '+' in request.POST.keys():
			b = request.POST['valueof']
			a=' + '
			s = b.split()
			k = len(s)
			if k == 1:
				return render(request,'news/calculator.html',locals())
			else:
				if s[1] == '+':
					b = int(s[0]) + int (s[2])
					a = ' + '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '-':
					b = int(s[0]) - int (s[2])
					a = ' + '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '*':
					b = int(s[0]) * int (s[2])
					a = ' + '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '/':
					b = float(s[0]) / float (s[2])
					a = ' + '
					return render(request,'news/calculator.html',locals())
			
		elif '-' in request.POST.keys():
			b = request.POST['valueof']
			a=' - '
			s = b.split()
			k = len(s)
			if k == 1:
				return render(request,'news/calculator.html',locals())
			else:
				if s[1] == '+':
					b = int(s[0]) + int (s[2])
					a = ' - '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '-':
					b = int(s[0]) - int (s[2])
					a = ' - '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '*':
					b = int(s[0]) * int (s[2])
					a = ' - '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '/':
					b = float(s[0]) / float (s[2])
					a = ' - '
					return render(request,'news/calculator.html',locals())
			
		elif '*' in request.POST.keys():
			b = request.POST['valueof']
			a=' * '
			s = b.split()
			k = len(s)
			if k == 1:
				return render(request,'news/calculator.html',locals())
			else:
				if s[1] == '+':
					b = int(s[0]) + int (s[2])
					a = ' * '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '-':
					b = int(s[0]) - int (s[2])
					a = ' * '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '*':
					b = int(s[0]) * int (s[2])
					a = ' * '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '/':
					b = float(s[0]) / float (s[2])
					a = ' * '
					return render(request,'news/calculator.html',locals())
			
		elif '/' in request.POST.keys():
			b = request.POST['valueof']
			a=' / '
			s = b.split()
			k = len(s)
			if k == 1:
				return render(request,'news/calculator.html',locals())
			else:
				if s[1] == '+':
					b = int(s[0]) + int (s[2])
					a = ' / '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '-':
					b = int(s[0]) - int (s[2])
					a = ' / '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '*':
					b = int(s[0]) * int (s[2])
					a = ' / '
					return render(request,'news/calculator.html',locals())
				elif s[1] == '/':
					b = float(s[0]) / float (s[2])
					a = ' / '
					return render(request,'news/calculator.html',locals())
			
		elif 'clear' in request.POST.keys():
			return HttpResponseRedirect('/calcu/')
		
		elif '=' in request.POST.keys():
			b = request.POST['valueof']
			s = b.split()
			if s[1] == '+':
				b = int(s[0]) + int (s[2])
				a = ''
				return calculat(request,b)
				
			elif s[1] == '-':
				b = int(s[0]) - int (s[2])
				a = ''
				return calculat(request)

			elif s[1] == '*':
				b = int(s[0]) * int (s[2])
				a = ''
				return calculat(request)

			elif s[1] == '/':
				b = float(s[0]) / float (s[2])
				a = ''
				return calculat(request)
	else:
		b = ''
		a = ''
		return render(request,'news/calculator.html',locals())

			
